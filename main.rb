require 'nokogiri'
require 'open-uri'
# require 'pry'

DOG_URL = 'http://www.multipet.com/products/dog-toys/all-categories'.freeze
STORE_DOMAIN = 'http://www.multipet.com'.freeze

doc = Nokogiri::HTML.parse(open(DOG_URL))

result = ''
doc.css('.product-item').each do |item|
  name = item.at_css('.product-name')&.text&.strip
  href = item.at_css('.thumb_link img')&.[]:src
  image_url = URI.join(STORE_DOMAIN, href)
  result << "#{name},#{image_url}\n"
end

File.write('toys.csv', result)
